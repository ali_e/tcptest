package main

import (
	"encoding/binary"
	"fmt"
	"net"
	"time"
)

const maxLengthBytes = 8

func client() {
	conn, err := net.Dial("tcp", "127.0.0.1:9999")
	if err != nil {
		fmt.Println(err)
		return
	}
	message := []string{"Hello from the darkest places, Hello from the darkest places, Hello from the darkest places, Hello from the darkest places, Hello from the darkest places"}
	message = append(message, "When the time calls for you, you should get up on your feet and go on")
	message = append(message, "The last warrior is the first who woke up")
	message = append(message, "sands and dusts are small, but they could slow you down")
	message = append(message, "Police doesn't know what to do, they just obey the law")
	message = append(message, "The pain you feel is the least thing you want")
	message = append(message, "The chair you sit on is your home")
	message = append(message, "Toes and whoes they hold rose")
	for _, text := range message {
		bs := make([]byte, maxLengthBytes)
		binary.LittleEndian.PutUint64(bs, uint64(len(text)))
		bytes := append(bs, []byte(text)...)
		conn.Write(bytes)
	}

	for {
		time.Sleep(time.Second * 2)
	}
}

func main() {

	// defer profile.Start(profile.MemProfile).Stop()
	listener, err := net.ListenTCP("tcp", &net.TCPAddr{Port: 9999})
	if err != nil {
		fmt.Println(err)
		return
	}
	go client()
	for {
		tcp, err := listener.AcceptTCP()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go Reader(tcp)
	}
}

func Reader(conn *net.TCPConn) {
	foundLength := false
	messageLength := 0
	for {
		if !foundLength {
			var b = make([]byte, maxLengthBytes)
			read, err := conn.Read(b)
			if err != nil {
				fmt.Println(err)
				continue
			}
			if read != 8 {
				fmt.Println("invalid header")
				continue
			}
			foundLength = true
			messageLength = int(binary.LittleEndian.Uint64(b))
		} else {
			var message = make([]byte, messageLength)
			read, err := conn.Read(message)
			if err != nil {
				fmt.Println(err)
				continue
			}
			if read != messageLength {
				fmt.Println("invalid data")
				continue
			}
			fmt.Println("Received:", string(message))
			foundLength = false
			messageLength = 0
		}
	}
}
